const express = require('express')
const { resolve } = require('path')
const history = require('connect-history-api-fallback')
const app = express()
const path = require('path');

const { PORT = 3000 } = process.env

// UI
const staticConf = { maxAge: '1y', etag: false }

app.use(express.static('dist', staticConf));
app.use(history({
    disableDotRule: true,
    verbose: true
}));
app.use(express.static('dist', staticConf));

app.get('/', function (req, res) {
    res.render('dist/index.html');
});



// Go
app.listen(PORT, () => console.log(`App running on port ${PORT}!`))

FROM node:erbium-slim

WORKDIR /app

COPY package.json /app/package.json
RUN yarn install
RUN yarn add @vue/cli
RUN yarn global add pm2
COPY . /app
RUN yarn build

EXPOSE 3000

#start app
CMD ["pm2-docker", "process.yml"]
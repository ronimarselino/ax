# Base code for Smart Analytic Platform front-end 

This code was auto-generated from vue-cli.
It does not contain unit testing nor E2E testing.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Build
docker build -t sap/web:latest .

module.exports = {
  // Note David : THIS IS NOT RECOMMENDED as apps that do not check the host are vulnerable to DNS rebinding attacks.
  devServer: {
    disableHostCheck: true,
  },
  "transpileDependencies": [
    "vuetify" 
  ],
  chainWebpack: config => config.plugins.delete('named-chunks')
}
# sed -i "s|<APP_ENVCONF>|$APP_ENVS|g" ./.env
# sed -i "s|<REDIS_HOSTCONF>|$REDIS_HOSTS|g" ./.env
# sed -i "s|<REDIS_PORTCONF>|$REDIS_PORTS|g" ./.env
# sed -i "s|<DB_MASTER_HOSTCONF>|$DB_MASTER_HOSTS|g" ./.env
# sed -i "s|<DB_PORTCONF>|$DB_PORTS|g" ./.env
# sed -i "s|<DB_USERNAMECONF>|$DB_USERNAMES|g" ./.env
# sed -i "s|<DB_PASSWORDCONF>|$DB_PASSWORDS|g" ./.env
# sed -i "s|<DB_SLAVE_HOSTCONF>|$DB_SLAVE_HOSTS|g" ./.env
# sed -i "s|<DB_NAMECONF>|$DB_NAMES|g" ./.env
aws ecr get-login --no-include-email --region <AWS_REGION_SGCONF> | /bin/bash
docker build -t <ECS_NAMECONF> .
docker tag <ECS_NAMECONF>:latest <ECRCONF>
docker push <ECRCONF>
aws ecs update-service --cluster <ECS_NAMECONF> --service <ECS_SERVICECONF> --force-new-deployment --region <AWS_REGION_SGCONF>
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Sign In Option',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/SignInOption.vue')
  },
  {
    path: '/newuserreg',
    name: 'NewUserReg',
    component: () => import('../views/NewUserReg.vue')
  },
  {
    path: '/existinguserreg',
    name: 'ExistingUserReg',
    component: () => import('../views/ExistingUserReg.vue')
  },
  {
    path: '/landingpage/',
    name: 'LandingPage',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/LandingPage.vue')
  },
  {
    path: '/landingpage/:congrats',
    name: 'LandingPageCongrats',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/LandingPage.vue')
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/Login.vue')
  },
  {
    path: '/bcilogin',
    name: 'BCILogin',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/BCILogin.vue')
  },
  {
    path: '/sendemailconfirmation',
    name: 'SendEmail',
    component: () => import('../views/SendEmailConfirmation.vue')
  },  
  {
    path: '/paymentmethod',
    name: 'PaymentMethod',
    component: () => import('../views/PaymentMethod')
  },
  {
    path: '/packageoffering',
    name: 'PackageOffering',
    component: () => import('../views/PackageOffering')
  },
  {
    path: '/resetpassword',
    name: 'ResetPassword',
    component: () => import('../views/ResetPassword')
  },
  {
    path: '/welcome',
    name: 'Welcome',
    component: () => import('../views/Welcome')
  },
  {
    path: '/subscriptionperiodselection',
    name: 'SubscriptionPeriod',
    component: () => import('../views/SubscriptionPeriodSelection')
  },
  {
    path: '/thankyou',
    name: 'ThankYou',
    component: () => import('../views/ThankYou')
  },
  {
    path: '/transfersummary',
    name: 'TransferSummary',
    component: () => import('../views/TransferSummary')
  },
  {
    path: '/paymentsuccessful',
    name: 'PaymentSuccessful',
    component: () => import('../views/PaymentSuccessful')
  },
  {
    path: '/confirmpayment',
    name: 'ConfirmPayment',
    component: () => import('../views/ConfirmPayment')
  },
  {
    path: '/thankyouconfirm',
    name: 'ThankYouConfirm',
    component: () => import('../views/ThankYouConfirm')
  },


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

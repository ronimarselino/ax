import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

const MY_ICONS = {
    subgroup: 'mdi-chevron-right',
    dropdown: 'mdi-chevron-up',
    checkboxOff: 'mdi-checkbox-blank-outline',
    checkboxOn: 'mdi-checkbox-marked-outline',
    radioOff: 'mdi-radiobox-blank',
    radioOn: 'mdi-radiobox-marked',
    cancel: 'mdi-close-circle',
  }

export default new Vuetify({
    icons: {
        iconfont: ['mdi', 'md'],
        values: MY_ICONS,
    }
});